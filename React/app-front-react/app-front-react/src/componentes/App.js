//componente raiz del proyecto

import React from  "react"; //importamos mod React
import logo from  "../logo.svg"; //importamos el fich
import './App.css';  //importamos el archivo css con los estilos
import CrearUsuario from "./CrearUsuario";  //importo CrearUsuario.js
import ListarUsuario from "./ListarUsuario"; //importo ListarUsuario.js


function App() {  // es un obj de JS con propiedades CSS
    let estiloLogo = {
        width:"10em" ,
        height:"10em"
    } //para aplicar el estilo hay que meterlo como propiedad del img mas abajo
    return(
        <Router>
        <div className="App">
            <header className="App-header">
                <img src={logo} 
                     style= {estiloLogo}
                     className="App-logo" 
                     alt= "logo"/> 
                {// lee el logo que ya hemos importado arriba
                }
                    <h1>Operaciones CRUD usuario</h1>
                
            </header>
            <nav>
                <link to="/">Listado</link> -
                <link to="registo">Crear Usuario</link>
            </nav>
            <Route path="/" exact component={listarUsuario}/>
            <Route path="registro" exact component={CrearUsuario}/>
        </div> 
        </Router>
        
        );
    }
    export default App;

                

           
            
